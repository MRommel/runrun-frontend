import { createStore, combineReducers, compose} from 'redux'

const userReducer = (state, action) => {
  switch(action.type) {
    case 'login': {
      return  { userInfo: action.user, token: action.token};
    }
    case 'logout': return null
    default: return state || null
  }
}

const modalReducer = (state, action) => {
  switch(action.type) {
    case 'showModal': return { type: action.modalType, extraData: action.extraData }
    case 'hideModal': return null
    default: return state || null
  }
}

const eventReducer = (state, action) => {
  switch(action.type) {
    case 'event' : return {type: action.event}
    default: return state || null
  }
}

const participantReducer = (state, action) => {
  switch(action.type) {
    case 'participant' : return {type: action.participant}
    default: return state || null
  }
}

const participantsReducer = (state, action) => {
  switch(action.type) {
    case 'participants' : return {type: action.participants}
    default: return state || null
  }
}

const participantsLoadedReducer = (state, action) => {
  switch(action.type) {
    case 'participantsLoaded' : return {type: action.participantsLoaded}
    default: return state || null
  }
}

const messageReducer = (state, action) => {
  switch(action.type) {
    case 'showMessage': return { type: action.messageType }
    case 'hideMessage': return null
    default: return state || null
  }
}

const rootReducer = combineReducers({
  user: userReducer,
  modal: modalReducer,
  event: eventReducer,
  participant: participantReducer,
  participants: participantsReducer,
  message: messageReducer,
  participantsLoaded: participantsLoadedReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

export default function generateStore () {
  const store = createStore (
    rootReducer,
    composeEnhancers()
  )
  return store
}











