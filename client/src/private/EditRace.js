import React, { useState }from 'react'
import { useDispatch, useSelector } from 'react-redux'

const EditRace = () => {
  
  const useFormField = () => {
    const [value, setValue] = useState('')
    return [value, e => setValue(e.target.value)]
  }
  
  const user = useSelector(s => s.user)
  const currentModal = useSelector(s => s.modal)
  const dispatch = useDispatch()
  const [raceName, setRaceName] = useFormField()
  const [council, setCouncil] = useFormField()
  const [raceDate, setRaceDate] = useFormField()
  const [startTime, setStartTime] = useFormField()
  const [isError, setError] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault()
    dispatch({ type: 'hideModal' })
    setError(false)

    try {
      const evento = { idEvent: currentModal.extraData.id_event, name: raceName, place: council, date: raceDate, time: startTime, id_organizer: user.userInfo.id_organizer,  };
      const ret = await fetch('http://localhost:3000/events', {
        method: 'PUT',
        body: JSON.stringify(evento),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + user.token 
        }
      })
      const event = await ret.json()     
      dispatch({ type: 'event', event });
      dispatch({type: 'showMessage', messageType: 'editRace'});
    } catch (err) {
      console.warn('Error:', err)
      setError(true)
    }
  }

  return (   
      <form className="login-form" onSubmit={handleSubmit}>
        <h3 className='modalHeading'>Introduce los nuevos datos de la carrera</h3>
        <div className="form-field">
          <label className="fields" for="raceName">Nombre de la carrera:</label>
          <input
            id="raceName"
            type="text"
            name="raceName"
            required
            placeholder={currentModal.extraData.event_name}
            value={raceName}
            onChange={setRaceName}
          />
        </div>
        <div className="form-field">
          <label className="fields" for="council">Lugar:</label>
          <input
            id="council"
            type="text"
            name="council"
            required
            placeholder={currentModal.extraData.place}
            value={council}
            onChange={setCouncil}
          />
        </div>
        <div className="form-field">
          <label className="fields" for='raceDate'>Fecha de la carrera:</label>
          <input
            id="raceDate"
            type="text"
            name="raceDate"
            required
            placeholder={currentModal.extraData.event_date}
            value={raceDate}
            onChange={setRaceDate}
          />
        </div>
        <div className="form-field">
          <label className="fields" for="startTime">Hora de comienzo:</label>
          <input
            id="startTime"
            type="text"
            name="startTime"
            required
            placeholder={currentModal.extraData.start_time}
            value={startTime}
            onChange={setStartTime}
          />
        </div>
        <div className="buttonsContainer">
          <button type="submit">Cambiar datos</button>
        </div>
        {isError && <div>Error, por favor inténtelo de nuevo</div>}
      </form >
  )
}

export default EditRace