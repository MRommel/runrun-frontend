import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

const Deliver = () => {
  const useFormField = () => {
    const [value, setValue] = useState('')
    return [value, e => setValue(e.target.value)]
  }
  const [taken_by, setTakenBy] = useFormField()
  const dispatch = useDispatch()
  const { token } = useSelector(s => s.user.token)
  const currentModal = useSelector(s => s.modal)
  const [isError, setError] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault()
    setError(false)
    const participant = currentModal.extraData;
    participant.delivered = true;
    participant.taken_by = taken_by;

    try {
      const ret = await fetch('http://localhost:3000/participants', {
        method: 'PUT',
        body: JSON.stringify(participant),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + token
        }
      })
      dispatch({ type: 'participant', participant });
      dispatch({ type: 'hideMessage' })
      dispatch({ type: 'showMessage', messageType: 'emailSentMessage' })
      dispatch({ type: 'hideModal' })
    } catch (err) {
      console.warn('Error:', err)
      setError(true)
    }
  }

  return (
    <form className="login-form" onSubmit={handleSubmit}>
      <h3 className='modalHeading'>Introduce el correo electrónico de la persona que recoge el dorsal</h3>
      <div className="form-field">
        <label for="taken_by">Email</label>
        <input id="taken_by" name="taken_by" type="email" required value={taken_by} onChange={setTakenBy} />
      </div>
      <div className="buttonsContainer">
        <button className="initSesion">Entregar</button>
      </div>
      {isError && <div>Error, por favor inténtelo de nuevo</div>}
    </form >
  )
}

export default Deliver