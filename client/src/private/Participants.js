import React from 'react';
import { useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import queryString from 'query-string';
import Message from '../messages/Message'
import LoadingSpinner from './LoadingSpinner'
import './Private.css';

const Participants = () => {
  const useFormField = () => {
    const [value, setValue] = useState('')
    return [value, e => setValue(e.target.value)]
  }

  const history = useHistory()
  const location = useLocation();
  const dispatch = useDispatch();
  const user = useSelector(s => s.user)
  const participantsLoaded = useSelector(s => s.participantsLoaded)
  const [name, setName] = useFormField()
  const [surname, setSurname] = useFormField()
  const [tutorEmail, setTutorEmail] = useFormField()
  const [dni, setDni] = useFormField()
  const [number, setNumber] = useFormField()
  const [color, setColor] = useFormField()
  const [numberType, setNumberType] = useFormField()
  const [email, setEmail] = useFormField()
  const [team, setTeam] = useFormField()
  const [participants, setParticipants] = useState()
  const participantDeliver = useSelector(s => s.participant);
  const [searched, setSearched] = useState(false);
  const [show, setShow] = useState(true);
  const [isError, setError] = useState(false);

  const handleSearch = async (e) => {
    e.preventDefault()
    setSearched(true);
    setShow(false);
    const idEvent = queryString.parse(location.search).idevent;
    const ret = await fetch(`http://localhost:3000/participants?participant_name=${name}&surname=${surname}&dni=${dni}&email=${email}&team=${team}&number=${number}&color=${color}&number_type=${numberType}&tutor_email=${tutorEmail}&idevent=${idEvent}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'bearer ' + user.token
      }
    })
    const jsonData = await ret.json()
    setParticipants(jsonData);
  }

  const handleEntregarDorsal = async (participant) => {
    dispatch({ type: 'showModal', modalType: 'deliver', 'extraData': participant });
  }

  const onInit = async () => {
    if (!user) {
      history.push('/')
    } else {
      try {
        const idEvent = queryString.parse(location.search).idevent
        const ret = await fetch(`http://localhost:3000/uploadedParticipants?idevent=${idEvent}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer ' + user.token
          }
        })
        participantsLoaded = await ret.json()
      } catch (err) {
        console.warn('Error:', err)
        setError(true)
      }
      try {
        const idEvent = queryString.parse(location.search).idevent
        const ret = await fetch(`http://localhost:3000/participants?idevent=${idEvent}`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer ' + user.token
          }
        })
        const jsonData = await ret.json().then()
        setParticipants(jsonData)
      } catch (err) {
        console.warn('Error:', err)
        setError(true)
      }

    }
  }

  const handleEditarParticipantes = (participant) => {
    dispatch({ type: 'showModal', modalType: 'editParticipant', 'extraData': participant })
  }

  const back = () => {
    history.push('/private');
    dispatch({ type: "hideMessage" })
  }

  const backToParticipants = () => {
    onInit();
    setSearched(false);
    setShow(true);
  }

  useEffect(() => {
    onInit();
  }, [participantDeliver, participantsLoaded]);

  if (participantsLoaded && participants) {
    return (
      <div className="private">
        <div className="container">
          <div className="searcher fadeIn">
            <form className="participants-login-form bounceInLeft">
              <div className="participants-form-field">
                <label className="fields" for="email">Email:</label>
                <input
                  id="email"
                  type="email"
                  name="email"
                  value={email}
                  onChange={setEmail}
                />
              </div>
              <div className="participants-form-field">
                <label className="fields" for="dni">DNI:</label>
                <input
                  id="dni"
                  type="text"
                  name="dni"
                  value={dni}
                  onChange={setDni}
                />
              </div>
              <div className="participants-form-field">
                <label className="fields" for='team'>Equipo:</label>
                <input
                  id="team"
                  type="text"
                  name="team"
                  value={team}
                  onChange={setTeam}
                />
              </div>
              <div className="participants-form-field">
                <label className="fields" for='number'>Dorsal:</label>
                <input
                  id="number"
                  type="text"
                  name="number"
                  value={number}
                  onChange={setNumber}
                />
              </div>
              <div className="participants-Buscar">
                <button onClick={handleSearch}>Buscar</button>
              </div>
            </form >
          </div>
          <div className="fadeInUpBig">
            <div><Message /></div>
            <div className="greetings-participants">
              <div>
                Puedes utilizar el buscador de arriba para encontrar al participante.
            </div>
              <div>
                Cuando lo hayas localizado, pulsa en <b>entregar dorsal</b>, para que la entrega quede marcada , y automáticamente se le enviará un email con los datos de su dorsal y de la recogida.
              </div>
            </div>
            <div className="events">
              <div className={show ? "showButtons" : "noButtons"}>
                <div className="buttonsContainer">
                  <button onClick={back}>Volver a las carreras</button>
                  <button className={searched ? "inSearch" : "noSearch"} onClick={backToParticipants}>Volver a la lista</button>
                </div>
              </div>
              <table>
                <thead>
                  <tr>
                    <th></th>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>DNI</th>
                    <th>Email</th>
                    <th>Dorsal</th>
                    <th>Equipo</th>
                    <th>Color</th>
                    <th>Tipo Dorsal</th>
                    <th>Email Tutor</th>
                    <th className="smallcell">Dorsal Entregado</th>
                    <th>Recogido por</th>
                  </tr>
                </thead>
                <tbody>
                  {participants.map(item => (
                    <tr className={item.delivered ? "delivered" : "noDelivered"}>
                      <td className="smallcell left"><button onClick={() => handleEditarParticipantes(item)}>editar</button></td>
                      <td>{item.participant_name}</td>
                      <td>{item.surname}</td>
                      <td>{item.dni}</td>
                      <td>{item.email}</td>
                      <td>{item.number}</td>
                      <td>{item.team}</td>
                      <td>{item.color}</td>
                      <td>{item.number_type}</td>
                      <td>{item.tutor_email}</td>
                      <td className="smallcell right"><input type="checkbox" checked={item.delivered} /></td>
                      <td className="smallcell right">
                        {
                          !item.delivered && (<button onClick={() => handleEntregarDorsal(item)}>Entregar Dorsal</button>)
                          || item.delivered && (item.taken_by)
                        }
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
            <div className="buttonsContainer">
              <button onClick={back}>Volver a las carreras</button>
              <button className={searched ? "inSearch" : "noSearch"} onClick={backToParticipants}>Volver a la lista</button>
            </div>
          </div>
        </div>
      </div>
    )
  } else {
    return (
      <div className="private">
        <div className="container">
          <div className="searcher fadeIn">
            <form className="participants-login-form bounceInLeft">
              <div className="participants-form-field">
                <label className="fields" for="email">Email:</label>
                <input
                  id="email"
                  type="email"
                  name="email"
                  value={email}
                  onChange={setEmail}
                />
              </div>
              <div className="participants-form-field">
                <label className="fields" for="dni">DNI:</label>
                <input
                  id="dni"
                  type="text"
                  name="dni"
                  value={dni}
                  onChange={setDni}
                />
              </div>
              <div className="participants-form-field">
                <label className="fields" for='team'>Equipo:</label>
                <input
                  id="team"
                  type="text"
                  name="team"
                  value={team}
                  onChange={setTeam}
                />
              </div>
              <div className="participants-form-field">
                <label className="fields" for='number'>Dorsal:</label>
                <input
                  id="number"
                  type="number"
                  name="number"
                  value={number}
                  onChange={setNumber}
                />
              </div>
              <div className="participants-Buscar">
                <button onClick={handleSearch}>Buscar</button>
              </div>
            </form >
          </div>
          <div className="fadeInUpBig">
            <div className="greetings">
              <LoadingSpinner/>
            </div>
            <div className="greetings">
              <b>Cargando participantes,</b> puede llevarnos un tiempo, por favor no recargues la página
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Participants