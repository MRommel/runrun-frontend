import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

const Event = () => {
  
  const useFormField = () => {
    const [value, setValue] = useState('')
    return [value, e => setValue(e.target.value)]
  }
  
  const user = useSelector(s => s.user)
  const dispatch = useDispatch()
  const [raceName, setRaceName] = useFormField()
  const [council, setCouncil] = useFormField()
  const [raceDate, setRaceDate] = useFormField()
  const [startTime, setStartTime] = useFormField()
  const [isError, setError] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault()
    dispatch({ type: 'hideModal' })
    setError(false)

    try {
      const newEvent = { name: raceName, place: council, date: raceDate, time: startTime, id_organizer: user.userInfo.id_organizer };
      const ret = await fetch('http://localhost:3000/events', {
        method: 'POST',
        body: JSON.stringify(newEvent),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + user.token 
        }
      })    
      let event = await ret.json() 
      dispatch({ type: 'event', event });
      dispatch({type: 'showMessage', messageType: 'createRace'})
    } catch (err) {
      console.warn('Error:', err)
      setError(true)
    }
  }

  return (   
      <form className="login-form" onSubmit={handleSubmit}>
        <h3 className='modalHeading'>Introduzca los datos de la carrera</h3>
        <div className="form-field">
          <label className="fields" for="raceName">Nombre de la carrera:</label>
          <input
            id="raceName"
            type="text"
            name="raceName"
            required
            value={raceName}
            onChange={setRaceName}
          />
        </div>
        <div className="form-field">
          <label className="fields" for="council">Lugar:</label>
          <input
            id="council"
            type="text"
            name="council"
            required
            value={council}
            onChange={setCouncil}
          />
        </div>
        <div className="form-field">
          <label className="fields" for='raceDate'>Fecha de la carrera:</label>
          <input
            id="raceDate"
            type="text"
            name="raceDate"
            required
            value={raceDate}
            onChange={setRaceDate}
          />
        </div>
        <div className="form-field">
          <label className="fields" for="startTime">Hora de comienzo:</label>
          <input
            id="startTime"
            type="text"
            name="startTime"
            required
            value={startTime}
            onChange={setStartTime}
          />
        </div>
        <div className="buttonsContainer">
          <button type="submit">Crear carrera</button>
        </div>
        {isError && <div>Error, por favor inténtelo de nuevo</div>}
      </form >
  )
}

export default Event