import React from 'react'
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux'

const DeleteRaceAlarm = () => {
  
  const user = useSelector(s => s.user)
  const currentModal = useSelector(s => s.modal)
  const event = useSelector(s => s.event)
  const dispatch = useDispatch()
  const [isError, setError] = useState(false)
  
  const handleBorrarCarrera = async () => {
    try {
      
      const ret = await fetch(`http://localhost:3000/events/` + currentModal.extraData.id_event, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + user.token
        }
      })
      dispatch({type: 'event', event})
      dispatch({type: 'showMessage', messageType: 'deleteRace'})
      dispatch({type: 'hideModal'})
    } catch (err) {
      console.error('Error:', err)
      setError(true)
    }
  }

  return (   
      <div className="login-form">
        <h3 className='modalHeading'>¿Estás segur@ de que quieres borrar la carrera?</h3>
        <div className="buttonsContainer">
          <button onClick={handleBorrarCarrera}>Borrar carrera</button>
        </div>
        {isError && <div>Error, por favor inténtelo de nuevo</div>}
      </div >
  )
}

export default DeleteRaceAlarm