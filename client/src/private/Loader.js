import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

const Loader = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const user = useSelector(s => s.user)
  const currentModal = useSelector(s => s.modal)
  const { token } = useSelector(s => s.user.token)
  const [isError, setError] = useState(false)
  const [file, setFile] = useState()

  const handleChange = (e) => {
    if (e.target.files[0]) {
      setFile({ file: e.target.files[0] })
    }
  }

  const handleSubmit = async (e) => {
    e.preventDefault()
    dispatch({ type: 'hideModal' })
    setError(false)

    const formData = new FormData()
    formData.append('file', file.file)

    try {
      const ret = await fetch('http://localhost:3000/upload?idevent=' + currentModal.extraData.id_event, {
        method: 'POST',
        body: formData,
        headers: {
          //'Content-Type': 'multipart/form-data',
          'Authorization': 'bearer ' + token
        }
      })
      const participantsLoaded = await ret.json()
      console.log(participantsLoaded)
      dispatch ({type: 'participantsLoaded', participantsLoaded})
      localStorage.getItem('token', user.token)
      history.push('/private/participants?idevent=' + currentModal.extraData.id_event)
    } catch (err) {
      console.warn('Error:', err)
      setError(true)
    }
  }

  return (
    <form className="login-form" onSubmit={handleSubmit}>
      <h3 className='modalHeading'>Introduce el archivo .csv con los datos de los participantes</h3>
      <div className="loader">
        <label for="file" className="file-text">Pulsa para subir el archivo </label>
        <input id="file" name="file" type="file" required onChange={handleChange} />
      </div>
      <div className="buttonsContainer">
        <button type="submit" className="initSesion">Guardar</button>
      </div>
      {isError && <div>Error, por favor inténtelo de nuevo</div>}
    </form >
  )
}

export default Loader