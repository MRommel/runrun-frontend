import React from 'react'
import { useSelector } from 'react-redux'
import DeleteRaceMessage from './DeleteRaceMessage'
import EditRaceMessage from './EditRaceMessage'
import CreateRaceMessage from './CreateRaceMessage'
import LoadParticipantsMessage from './LoadParticipantsMessage'
import DeleteProfileMessage from './DeleteProfileMessage'
import EmailSentMessage from './EmailSentMessage'
import './Message.css'

const Message = () => {
  const currentMessage = useSelector(s => s.message)
  if (!currentMessage) return false

  let Words
  if (currentMessage.type === 'deleteRace') Words = DeleteRaceMessage
  if (currentMessage.type === 'editRace') Words = EditRaceMessage
  if (currentMessage.type === 'createRace') Words = CreateRaceMessage
  if (currentMessage.type === 'loadParticipants') Words = LoadParticipantsMessage
  if (currentMessage.type === 'deleteProfile') Words = DeleteProfileMessage
  if (currentMessage.type === 'emailSentMessage') Words = EmailSentMessage



  return (
    <div className="puff-in-center">
      <div className="mensaje">
        <Words />
      </div>
    </div>
  )
}

export default Message


