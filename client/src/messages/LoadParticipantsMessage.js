import React from 'react'
import { useSelector } from 'react-redux'

const LoadParticipantsMessage = () => {
  return (<div>Acabas de introducir participantes, ahora pulsa sobre <b>ver participantes</b> para acceder al buscador</div>)
}

export default LoadParticipantsMessage