import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

const useFormField = () => {
  const [value, setValue] = useState('')
  return [value, e => setValue(e.target.value)]
}

const EditProfile = () => {
  const dispatch = useDispatch()
  const history = useHistory()
  const editUser = useSelector(s => s.user)
  const [newName, setNewName] = useFormField()
  const [newSurname, setNewSurname] = useFormField()
  const [newEmail, setNewEmail] = useFormField()
  const [newPassword, setNewPassword] = useFormField()
  const [isError, setError] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault()
    setError(false)
    try {
      
      const newUser = { name: newName, surname: newSurname, email: newEmail, password: newPassword, id_organizer: editUser.userInfo.id_organizer }
      const ret = await fetch('http://localhost:3000/register', {
        method: 'PUT',
        body: JSON.stringify(newUser),
        headers: {
          'Content-Type': 'application/json'
          // 'Authorization': localStorage.getItem('token') // Esto en todas las llamadas autenticadas
        }
      })
      const {user, token} = await ret.json()
      dispatch({type: 'login', user, token})
      dispatch({ type: 'hideModal' })
      history.push('/private')


      //localStorage.setItem('token', data.token) // Esto solo en login, para guardar el token
      //history.push(`/users/${data.id}`)
    } catch (err) {
      console.warn('Error:', err)
      setError(true)
    }
  }

  return (
    <form className="login-form" onSubmit={handleSubmit}>
      <h3 className='modalHeading'>Cambia los datos de tu perfil de usuari@</h3>
      <div className="form-field-login">
        <label className="fields" for="name">Nombre:</label>
        <input
          id="name"
          type="text"
          name="name"
          placeholder={editUser.userInfo.organizer_name}
          required
          value={newName}
          onChange={setNewName}
        />
      </div>
      <div className="form-field-login">
        <label className="fields" for="surname">Apellidos:</label>
        <input
          id="surname"
          type="text"
          name="surname"
          placeholder={editUser.userInfo.surname}
          required
          value={newSurname}
          onChange={setNewSurname}
        />
      </div>
      <div className="form-field-login">
        <label className="fields" for='email'>Email:</label>
        <input
          id="email"
          type="email"
          name="email"
          placeholder={editUser.userInfo.email}
          required
          value={newEmail}
          onChange={setNewEmail}
        />
      </div>
      <div className="form-field-login">
        <label className="fields" for="pass">Password:</label>
        <input
          id="pass"
          type="password"
          name="password"
          placeholder="Introduce tu nueva contraseña"
          required
          value={newPassword}
          onChange={setNewPassword} />
      </div>
      <div className="buttonsContainer-login">
        <button className="registrate">Guardar cambios</button>
      </div>
      {isError && <div>Error, por favor inténtelo de nuevo</div>}
    </form >
  )
}


export default EditProfile