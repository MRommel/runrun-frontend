import React from 'react'
import { useState } from 'react';
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'

const DeleteProfile = () => {
  
  const user = useSelector(s => s.user)
  const dispatch = useDispatch()
  const history = useHistory()
  const [isError, setError] = useState(false)
  
  const handleBorrarPerfil = async () => {
    try {
      
      const ret = await fetch(`http://localhost:3000/organizer/` + user.userInfo.id_organizer, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + user.token
        }
      })
      dispatch({type: 'logout'})
      dispatch({type: 'hideModal'})
      history.push('/')
      dispatch({type: 'showMessage', messageType: 'deleteProfile'})

    } catch (err) {
      console.error('Error:', err)
      setError(true)
    }
  }

  return (   
      <div className="login-form">
        <h3 className='modalHeading'>¿Estás segur@ de que quieres eliminar tu perfil?</h3>
        <p className="noRegMessage"><b>¡Estás a punto de eliminar tu perfil!</b> Antes de hacerlo asegúrate de que no tienes ninguna carrera activa en este momento</p>
        <div className="buttonsContainer">
          <button onClick={handleBorrarPerfil}>Elimininar perfil</button>
        </div>
        {isError && <div>Error, por favor inténtalo de nuevo</div>}
      </div >
  )
}

export default DeleteProfile