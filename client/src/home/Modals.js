import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import SignIn from './SignIn'
import SignUp from './SignUp'
import Event from '../private/Event'
import Loader from '../private/Loader'
import Deliver from '../private/Deliver'
import EditRace from '../private/EditRace'
import EditProfile from './EditProfile'
import DeleteRaceAlarm from '../private/DeleteRaceAlarm'
import DeleteProfile from './DeleteProfile'
import EditParticipant from '../private/EditParticipant'
import './Modals.css'

const Modals = () => {
  const dispatch = useDispatch()
  const currentModal = useSelector(s => s.modal)
  if (!currentModal) return false
  
  const handleClose = () => dispatch({ type: 'hideModal' })
  const handleClick = (e) => e.stopPropagation()

  let Modal
  if (currentModal.type === 'signin') Modal = SignIn
  if (currentModal.type === 'signup') Modal = SignUp
  if (currentModal.type === 'event') Modal = Event
  if (currentModal.type === 'loader') Modal = Loader
  if (currentModal.type === 'deliver') Modal = Deliver
  if (currentModal.type === 'editRace') Modal = EditRace
  if (currentModal.type === 'editProfile') Modal = EditProfile
  if (currentModal.type === 'deleteRaceAlarm') Modal = DeleteRaceAlarm
  if (currentModal.type === 'deleteProfile') Modal = DeleteProfile
  if (currentModal.type === 'editParticipant') Modal = EditParticipant


  return (
    <div className="modal-background" onClick={handleClose}>
      <div className="modal-foreground zoomIn" onClick={handleClick}>
      <div className="closing-div">
      <a class="close" onClick={handleClose}></a>
      </div>
          <div className="signin-form">
            <Modal />
          </div>
      </div>
    </div>
  )
}

export default Modals



